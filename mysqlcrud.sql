-- creating a database
create database reshma;
use reshma;
-- create table
create table reshu(id int, name varchar(20), domain varchar(20));
insert into reshu(id, name, domain) values(1,"ravi","pythonbigdata" );
insert into reshu(id,name, domain) values(2,"chaitu","javabigdata"); 
show databases;
describe reshu;
-- read records
select*from reshu;
select id from reshu;
select id,name from reshu where domain="pythonbigdata";
-- update records
update reshu 
set name="rams"
where id=1;
describe reshu;
alter table reshu rename to resh;
alter table resh add phno varchar(20);
alter table resh drop phno;
-- delete table
-- drop table resh;
-- drop database reshma;
-- delete from resh where id=2;

