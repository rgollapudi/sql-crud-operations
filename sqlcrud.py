# import mysql.connector which connects sql to python
import mysql.connector
# give information of your database
database=mysql.connector.connect(
    host="localhost",
    user="root",
    password="Hello@1234",
    database="reshma"
)
mycursor = database.cursor()
print(database)
print("0-create a table")
print("1-insert a record")
print("2-read the database")
print("3-see the tables in database")
print("4-read a table")
print("5-read records  ")
print("6-rename table")
print("7-add a column")
print("8-drop a column")
print("9-update a record")
print("10-delete a database")
print("11-delete a record")
num=int(input("enter the operation you want to perform"))
 # it creates a table
def create_table():
    mycursor.execute("create table employee(id int,name varchar(20),domain varchar(20)")
    print("created")
    return
# create_table()

# enter values to the table
def insert_values():
    mycursor = database.cursor()
    val =[ ("1", "ravi","pythonbd"),("2", "chay","javabd")]
    mycursor.execute("INSERT INTO employee (id,name, domain) VALUES (%s,%s, %s)",  val)
    database.commit()
    print(mycursor.rowcount, "record inserted.")
# insert_values()

# shows all the databases
def read_database():
    mycursor.execute("SHOW DATABASES")
    for db in mycursor:
     print(db)
# read_database()

# shows all the tables in the database
def show_table():
    mycursor.execute("SHOW TABLES")
    for y in mycursor:
        print(y)

# it reads the tables
def read_table():
    mycursor.execute("DESCRIBE studentdetails")
    for y in mycursor:
     print(y)
# read_table()

# it reads all the entries in table
def read_records():
    mycursor.execute("SELECT * FROM studentdetails")
    for records in mycursor:
     print(records)
# read_records()

# changes name of table
def rename_table():
    mycursor.execute("change table employee To empdetails")
    print('name changed')
# rename_table()

# is adds new column to the table
def add_column():
     mycursor.execute("change table empdetails ADD phno varchar(255)")
     print("succesfully added")
# add_column()

# it drops column from the table
def drop_column():
		mycursor = database.cursor()
		mycursor.execute("change table empdetails drop column phno")
		print("column is deleted")
# drop_column()

# it updates value to new value
def update_record():
     mycursor.execute("UPDATE empdetails set name='reshu' WHERE  id=1")
     print("succesfully updated")
# update_record()

# it permanently deletes database
def delete_database():
    mycursor.execute("drop database reshma")
    print('database deleted succesfully')
# delete_database()

# deletes selected records
def delete_record():
    mycursor.execute("DELETE from empdetails where id=1")
    print("record deleted")
# delete_record()

# select an entry to call a function
if num==0:
  create_table()
if num==1:
  insert_values()
if num==2:
  read_database()
if num==3:
  show_table()
if num==4:
  read_table()
if num==5:
  read_records()
if num==6:
  rename_table()
if num==7:
  add_column()
if num==8:
  drop_column()
if num==9:
  update_record()
if num==10:
  delete_database()
if num==11:
  delete_record()



